FROM node:lts-alpine as client-builder

# install simple http server for serving static content
RUN npm install -g http-server

# make the 'app' folder the current working directory
WORKDIR /app

# copy both 'package.json' and 'package-lock.json' (if available)
COPY covid-client/package*.json ./

# install project dependencies
RUN npm install

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY covid-client/. .

# build app for production with minification
RUN npm run build

FROM ubuntu:18.04 as tiles-builder

# Update repos and install dependencies
RUN apt-get update \
  && apt-get -y upgrade \
  && apt-get -y install wget git build-essential libsqlite3-dev zlib1g-dev

# Build tippecanoe
RUN mkdir -p /tmp/tippecanoe-src
RUN git clone https://github.com/mapbox/tippecanoe.git /tmp/tippecanoe-src
WORKDIR /tmp/tippecanoe-src
RUN make \
  && make install

# Back to root dir
WORKDIR /

# Fetch the desired geojson
RUN wget -O gemeentes.geojson "https://service.pdok.nl/cbs/wb2021/wfs/v1_0?request=GetFeature&service=WFS&version=1.1.0&typeName=gemeenten&outputFormat=application%2Fjson%3B%20subtype%3Dgeojson&srsName=urn:ogc:def:crs:EPSG::4326"

# Transform into tiles
RUN tippecanoe -e tiles gemeentes.geojson \
  -y gemeentecode -y gemeentenaam -y aantalInwoners \
  -y bevolkingsdichtheidInwonersPerKm2 -y water \
  --no-tile-compression \
  -Z5 -z10 -j '{ "*": [ "!=", "water", "JA" ] }'


# Assemble final image
FROM alpine

# Copy static files to target
COPY --from=client-builder /app/dist /web
COPY --from=tiles-builder /tiles /web/tiles

RUN apk add tree
RUN tree /web
