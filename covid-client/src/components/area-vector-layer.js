import MVT from 'ol/format/MVT';
import VectorTileLayer from 'ol/layer/VectorTile';
import VectorTileSource from 'ol/source/VectorTile';
import { Style, Fill, Stroke } from 'ol/style';
import * as legend from './legend.js'

export default class AreaVectorLayer {
  map = null;
  dictionary = new Map();
  mode = "perPopulation";
  scale = "lin";
  max = 0;
  municipalities = new Map();

  constructor(getHoverKey) {
    this.layer = new VectorTileLayer({
      opacity: 0.8,
      style: this.getStyleFunction(getHoverKey),
      source: new VectorTileSource({
        format: new MVT(),
        minZoom: 5,
        maxZoom: 10,
        url: '/tiles/{z}/{x}/{y}.pbf',
      }),
    });
  }

  getLayer() {
    return this.layer;
  }

  setMap(map) {
    this.map = map;
  }

  updateLegend(legendInformation) {
    legendInformation.forEach(v => {
      v.selected = legend.createLegendSelectedStyle(v.color);
      v.style = legend.createLegendStyle(v.color);
      v.hovered = legend.createLegendHoveredStyle(v.color);
      v.hoveredSelected = legend.createLegendHoveredSelectedStyle(v.color);
    });
    this.legend = legendInformation;
  }

  getStyleFunction(getHoverKey) {
    return (feature) => {
      let props = feature.getProperties();
      this.index(props);

      let code = props.gemeentecode;
      let isHovered = code == getHoverKey.apply();

      if (this.dictionary == null || !this.dictionary.has(code)) {
        return new Style({
          fill: new Fill({ color: "#888" }),
          stroke: isHovered ? new Stroke({ width: 2, color: "black" }) : new Stroke({ width: 1, color: "white" }),
          zIndex: isHovered ? 1 : 0
        });
      }

      let result = this.dictionary.get(code);
      let population = props.aantalInwoners;
      let populationDensity = props.bevolkingsdichtheidInwonersPerKm2;
      let value = this.getValue(result, population, populationDensity);

      let sigmaMax = this.getMax();

      let resultPerc = this.scale == "log"
        ? (value - this.min) / (sigmaMax - this.min)
        : value / sigmaMax;

      if (isHovered) {
        console.log(code + " > " + result + " > " + resultPerc);
      }
      return new Style({
        fill: new Fill({ color: this.getColor(resultPerc) }),
        stroke: isHovered ? new Stroke({ width: 2, color: "black" }) : new Stroke({ width: 1, color: "white" }),
        zIndex: isHovered ? 1 : 0
      });
    }
  }

  getMax() {
    switch (this.scale) {
      case "lin":
        return this.max;
      case "log":
        return Math.log(1 + this.max)
      default:
        return 0;
    }
  }

  getValue(amount, pop, popPerKm2) {
    switch (this.mode) {
      case "absolute":
        return amount;
      case "perPopulation":
        return amount / pop;
      case "perSurface":
        return amount / (pop / popPerKm2);
    }

    return 0;
  }

  setScale(scale) {
    this.scale = scale;
    this.layer.changed();
  }

  setMode(mode) {
    this.mode = mode;
    this.updateMax();
    this.updateTotal();
  }

  updateData(dictionary) {
    this.dictionary = dictionary;
    this.updateMax();
    this.updateTotal();
  }

  updateTotal() {
    if (this.dictionary == null) {
      return;
    }

    let sum = Array.from(this.dictionary.values())
      .reduce((acc, val) => acc + val, 0);
    const event = new CustomEvent("sum-update", { detail: sum });
    this.map.target.dispatchEvent(event);
  }

  updateMax() {
    if (this.dictionary == null) {
      return;
    }

    this.max = Array.from(this.municipalities.values())
      .filter(v => this.dictionary.has(v.gemeentecode))
      .map(v => this.getValue(this.dictionary.get(v.gemeentecode),
            v.aantalInwoners,
            v.bevolkingsdichtheidInwonersPerKm2))
      .reduce((a, b) => Math.max(a, b), 0);
    this.min = Array.from(this.municipalities.values())
      .filter(v => this.dictionary.has(v.gemeentecode))
      .map(v => this.getValue(this.dictionary.get(v.gemeentecode),
            v.aantalInwoners,
            v.bevolkingsdichtheidInwonersPerKm2))
      .filter(v => v != 0)
      .reduce((a, b) => Math.max(0, Math.min(a, b)), Number.MAX_SAFE_INTEGER);

    const event = new CustomEvent("max-update", { detail: this.max });
    this.map.target.dispatchEvent(event);
    this.layer.changed();
  }

  changed() {
    this.layer.changed();
  }

  getMode() {
    return this.mode;
  }

  getColor(value) {
    let valueBounded = Math.max(0, Math.min(1, value));
    //value from 0 to 1
    var hue = ((1 - valueBounded) * 120).toString(10);
    return "hsl(" + hue + ",100%,50%)";
  }

  index(props) {
    if (!this.municipalities.has(props.gemeentecode)) {
      this.municipalities.set(props.gemeentecode, props);
    }
  }
}
