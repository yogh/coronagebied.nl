import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import { defaults as defaultControls } from 'ol/control';

import { createBackgroundLayer, createLabelLayer } from './extra-layers';
import AreaVectorLayer from './area-vector-layer'

export default class AreaVector {
  lastHoverKey = "";

  constructor(target) {
    let bg = createBackgroundLayer();
    let labels = createLabelLayer();
    this.layer = new AreaVectorLayer(() => this.lastHoverKey);

    this.target = target;
    this.map = new Map({
      target,
      controls : defaultControls({
        attribution : false,
        zoom : false,
      }),
      view: new View({
        center: [608765, 6834026],
        extent: [356222.37053847546, 6577190.191650908, 803726.7235274351, 7113543.379420633],
        constrainOnlyCenter: true,
        zoom: 8,
        minZoom: 6,
        maxZoom: 13,
      }),
      layers: [ bg, this.layer.getLayer(), labels ],
    });

    this.layer.setMap(this);

    this.map.on('pointermove', e => {
      const features = this.map.getFeaturesAtPixel(e.pixel);
      if (features.length == 0) {
        this.map.getViewport().style.cursor = '';
        return;
      }

      const properties = features[0].getProperties();
      this.map.getViewport().style.cursor = 'pointer';

      if (this.lastHoverKey === properties.gemeentecode) {
        return;
      }

      this.lastHoverKey = properties.gemeentecode;
      if (this.hoverCallback != null) {
        this.hoverCallback.apply(this, [ properties ]);
        this.layer.changed();
      }
    });
  }

  draw(dictionary) {
    this.layer.updateData(dictionary);
  }

  setScale(scale) {
    this.layer.setScale(scale);
  }

  setMode(mode) {
    this.layer.setMode(mode);
  }

  getMode() {
    return this.layer.getMode();
  }

  onHoverFeature(hoverCallback) {
    this.hoverCallback = hoverCallback;
  }
}
