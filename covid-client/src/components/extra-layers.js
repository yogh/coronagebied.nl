import TileLayer from 'ol/layer/Tile';
import WMTS from 'ol/source/WMTS';
import WMTSTileGrid from 'ol/tilegrid/WMTS';
import { get as getProjection } from 'ol/proj';
import { getTopLeft, getWidth } from 'ol/extent';

import proj4 from 'proj4';
import { register } from 'ol/proj/proj4';

export function createLabelLayer() {
  proj4.defs("EPSG:28992", "+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +towgs84=565.417,50.3319,465.552,-0.398957,0.343988,-1.8774,4.0725 +units=m +no_defs");
  register(proj4);
  let projection = getProjection('EPSG:28992');
  
  const projectionExtent = [  -285401.920, 22598.080, 595401.920, 903401.920 ];
  const size = getWidth(projectionExtent) / 256;
  const resolutions = new Array(19);
  const matrixIds = new Array(19);
  for (let z = 0; z < 19; ++z) {
    // generate resolutions and matrixIds arrays for this WMTS
    resolutions[z] = size / Math.pow(2, z);
    matrixIds[z] = z;
  }

  return new TileLayer({
    source: new WMTS({
      url: 'https://geodata.nationaalgeoregister.nl/tiles/service/wmts',
      layer: 'lufolabels',
      matrixSet: 'EPSG:28992',
      format: 'image/png',
      projection: projection,
      minZoom: 8,
      tileGrid: new WMTSTileGrid({
        origin: getTopLeft(projectionExtent),
        resolutions: resolutions,
        matrixIds: matrixIds,
      }),
      style: 'default',
    }),
  });
}

export function createBackgroundLayer() {
  let projection = getProjection('EPSG:3857');
  const projectionExtent = projection.getExtent();
  const size = getWidth(projectionExtent) / 256;
  const resolutions = new Array(19);
  const matrixIds = new Array(19);
  for (let z = 0; z < 19; ++z) {
    // generate resolutions and matrixIds arrays for this WMTS
    resolutions[z] = size / Math.pow(2, z);
    matrixIds[z] = z;
  }

  return new TileLayer({
    source: new WMTS({
      url: 'https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0',
      layer: 'grijs',
      matrixSet: 'EPSG:3857',
      format: 'image/png',
      projection: projection,
      minZoom: 3,
      tileGrid: new WMTSTileGrid({
        origin: getTopLeft(projectionExtent),
        resolutions: resolutions,
        matrixIds: matrixIds,
      }),
      style: 'default',
    }),
  });
}