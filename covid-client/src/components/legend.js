import { Style, Fill, Stroke } from 'ol/style';

export function findLegendItem(num, legend) {
  let lastItem;
  for (const item of legend) {
    if (lastItem == null) {
      lastItem = item;
      continue;
    } else if (item.min >= num) {
      return lastItem;
    } else {
      lastItem = item;
    }
  }
  
  return lastItem;
}

export const none = new Style({
  fill: new Fill({ color: "transparent" }),
  stroke: new Stroke({ width: 1, color: "#555" }),
  zIndex: 0
});
export const noneHovered = new Style({
  fill: new Fill({ color: "transparent" }),
  stroke: new Stroke({ width: 4, color: "black" }),
  zIndex: 2
});
export const noneSelected = new Style({
  fill: new Fill({ color: "#0003" }),
  stroke: new Stroke({ width: 2, color: "black" }),
  zIndex: 1
});
export const noneSelectedHovered = new Style({
  fill: new Fill({ color: "#0003" }),
  stroke: new Stroke({ width: 4, color: "black" }),
  zIndex: 2
});

export function createLegendStyle(clr) {
  return clr == null ? null : new Style({
    stroke: new Stroke({ width: 1, color: "#888" }),
    fill: new Fill({ color: clr }),
    zIndex: 0
  });
}

export function createLegendHoveredStyle(clr) {
  return clr == null ? null : new Style({
    stroke: new Stroke({ width: 4, color: "black" }),
    fill: new Fill({ color: clr }),
    zIndex: 2
  });
}

export function createLegendHoveredSelectedStyle(clr) {
  return clr == null ? null : new Style({
    stroke: new Stroke({ width: 4, color: "black" }),
    fill: new Fill({ color: clr }),
    zIndex: 2
  });
}

export function createLegendSelectedStyle(clr) {
  return clr == null ? null : new Style({
    stroke: new Stroke({ width: 2, color: "black" }),
    fill: new Fill({ color: clr }),
    zIndex: 1
  });
}