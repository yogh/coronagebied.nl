import Vue from 'vue'
import VueRouter from 'vue-router'
import MapView from '../views/MapView.vue'
import InfoView from '../views/InfoView.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', component: MapView },
  { path: '/positive', component: MapView },
  { path: '/hospital', component: MapView },
  { path: '/fatal', component: MapView },
  { path: '/about', component: InfoView },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
