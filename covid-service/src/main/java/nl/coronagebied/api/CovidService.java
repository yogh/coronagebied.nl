package nl.coronagebied.api;

import java.time.LocalDate;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nl.coronagebied.repository.AbstractMunicipalityValueRepository;
import nl.coronagebied.repository.ActiveCasesRepository;
import nl.coronagebied.repository.CumulativeCasesRepository;
import nl.coronagebied.repository.DeltaCasesRepository;

@RestController("/api")
public class CovidService {
  @Autowired
  DeltaCasesRepository deltaCasesRepository;
  @Autowired
  CumulativeCasesRepository cumulativeCasesRepository;
  @Autowired
  ActiveCasesRepository activeCasesRepository;

  @GetMapping("delta-cases")
  public Set<Entry<LocalDate, Map<String, Integer>>> getDeltaCases(
      @RequestParam(value = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
      @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return smartBetween(startDate, endDate, deltaCasesRepository);
  }

  @GetMapping("total-cases")
  public Set<Entry<LocalDate, Map<String, Integer>>> getTotalCases(
      @RequestParam(value = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
      @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return smartBetween(startDate, endDate, cumulativeCasesRepository);
  }

  @GetMapping("active-cases")
  public Set<Entry<LocalDate, Map<String, Integer>>> getActiveCases(
      @RequestParam(value = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
      @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return smartBetween(startDate, endDate, activeCasesRepository);
  }

  private Set<Entry<LocalDate, Map<String, Integer>>> smartBetween(LocalDate startDate, LocalDate endDate, AbstractMunicipalityValueRepository repo) {
    if (startDate == null && endDate == null) {
      return repo.getValues()
          .entrySet();
    } else if (startDate == null && endDate != null) {
      return repo.getValuesUntil(endDate)
          .entrySet();
    } else if (startDate != null && endDate == null) {
      return repo.getValuesSince(startDate)
          .entrySet();
    } else {
      return repo.getValuesBetween(startDate, endDate)
          .entrySet();
    }
  }
}
