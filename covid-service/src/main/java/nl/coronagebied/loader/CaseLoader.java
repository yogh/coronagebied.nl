package nl.coronagebied.loader;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import com.google.common.collect.EvictingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;
import nl.coronagebied.repository.ActiveCasesRepository;
import nl.coronagebied.repository.CumulativeCasesRepository;
import nl.coronagebied.repository.DeltaCasesRepository;

@Component
public class CaseLoader {
  private static final Logger LOG = LoggerFactory.getLogger(CumulativeCasesRepository.class);

  public static final String CASES_CUMULATIVE_ENDPOINT_URL = "https://data.rivm.nl/covid-19/COVID-19_aantallen_gemeente_cumulatief.json";

  private static final int EVICTION_SIZE = 2 * 7;

  @Autowired
  CumulativeCasesRepository cumulativeCasesRepository;
  @Autowired
  DeltaCasesRepository deltaCasesRepository;
  @Autowired
  ActiveCasesRepository activeCasesRepository;

  public void retrieveCumulativeCases() {
    LOG.info("Retrieving cumulative cases from {}", CASES_CUMULATIVE_ENDPOINT_URL);

    JSONArray json = Unirest.get(CASES_CUMULATIVE_ENDPOINT_URL)
        .asJson()
        .getBody()
        .getArray();

    reset();

    List<JSONObject> lst = new ArrayList<>();
    json.forEach(node -> lst.add((JSONObject) node));
    lst.stream()
        .filter(v -> !v.isNull("Municipality_code")) // Filter out provinces
        .forEach(node -> index(node));

    Map<LocalDate, Map<String, Integer>> cumulativeCases = cumulativeCasesRepository.getValues();

    Map<String, Integer> previousCumulative = new HashMap<>();
    for (Entry<LocalDate, Map<String, Integer>> entry : cumulativeCases.entrySet()) {
      final Map<String, Integer> previousCumulativeFinal = previousCumulative;
      entry.getValue().forEach(
          (k, v) -> deltaCasesRepository.put(entry.getKey(), k, v - previousCumulativeFinal.getOrDefault(k, 0)));

      previousCumulative = entry.getValue();
    }

    Map<String, EvictingQueue<Integer>> activeCases = new HashMap<>();
    Map<LocalDate, Map<String, Integer>> values = deltaCasesRepository.getValues();
    values.forEach((k, v) -> v.forEach((mun, delta) -> {
      activeCases.putIfAbsent(mun, EvictingQueue.create(EVICTION_SIZE));
      EvictingQueue<Integer> queue = activeCases.get(mun);
      queue.add(delta);
      activeCasesRepository.put(k, mun, queue.stream().mapToInt(val -> val).sum());
    }));

    LOG.info("Done.");
  }

  private void reset() {
    cumulativeCasesRepository.clear();
    deltaCasesRepository.clear();
    activeCasesRepository.clear();

  }

  private void index(JSONObject node) {
    String dateTime = node.getString("Date_of_report");
    String municipalityCode = node.getString("Municipality_code");
    int reported = node.getInt("Total_reported");

    String date = dateTime.split(" ")[0];
    LocalDate localDate = LocalDate.parse(date);

    cumulativeCasesRepository.put(localDate, municipalityCode, reported);
  }
}
