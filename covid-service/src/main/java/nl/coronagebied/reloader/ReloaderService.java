package nl.coronagebied.reloader;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kong.unirest.Unirest;
import nl.coronagebied.loader.CaseLoader;

@Component
public class ReloaderService {
  private static final Logger LOG = LoggerFactory.getLogger(ReloaderService.class);

  // Default to 2 weeks ago
  private ZonedDateTime previous = ZonedDateTime.now().minusDays(14);

  @Autowired
  CaseLoader loader;

  @PostConstruct
  public void init() {
    final ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
    ZonedDateTime nextRun = now.withHour(15).withMinute(14).withSecond(40);
    if (now.compareTo(nextRun) > 0) {
      nextRun = nextRun.plusDays(1);
    }

    final Duration initalDelay = Duration.between(now, nextRun);
    final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    scheduler.scheduleAtFixedRate(() -> fetchData(),
        initalDelay.getSeconds(),
        TimeUnit.DAYS.toSeconds(1),
        TimeUnit.SECONDS);

    LOG.info("Scheduled daily auto-updates. Performing update in a few minutes.");
    LOG.info("Next scheduled update happens {} minutes ({} hours) from now.",
        initalDelay.get(ChronoUnit.SECONDS) / 60,
        initalDelay.get(ChronoUnit.SECONDS) / 60 / 60);

    new Thread(() -> {
      try {
        Thread.sleep(10_000);
      } catch (InterruptedException e) {}
      fetchData();
    }).start();
  }

  public void fetchData() {
    LOG.info("Performing auto-update on {}", ZonedDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
    String lastModified = Unirest.head(CaseLoader.CASES_CUMULATIVE_ENDPOINT_URL)
        .asString()
        .getHeaders()
        .getFirst("Last-Modified");

    ZonedDateTime zdt = ZonedDateTime.parse(lastModified, DateTimeFormatter.RFC_1123_DATE_TIME);

    LOG.info("Last-Modified: {} (previous: {})",
        zdt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
        previous.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

    if (zdt.isAfter(previous)) {
      LOG.info("Remote file is more recent than last known version.");
      previous = zdt;
      loader.retrieveCumulativeCases();
    } else {
      LOG.warn("No new data discovered. Trying again in 10 minutes.");
      final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
      scheduler.schedule(() -> fetchData(),
          10, TimeUnit.MINUTES);
    }
  }
}
