package nl.coronagebied.repository;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

@Component
public abstract class AbstractMunicipalityValueRepository {
  private Map<LocalDate, Map<String, Integer>> values = new TreeMap<>();

  public void put(LocalDate date, String code, int amount) {
    values.putIfAbsent(date, new HashMap<>());
    Map<String, Integer> caseMap = values.get(date);
    caseMap.put(code, amount);
  }

  public Map<LocalDate, Map<String, Integer>> getValues() {
    return values;
  }

  public void clear() {
    values.clear();
  }

  public Map<LocalDate, Map<String, Integer>> getValuesSince(LocalDate startDate) {
    return values.entrySet().stream()
        .filter(v -> v.getKey().isAfter(startDate.minusDays(1)))
        .collect(Collectors.toMap(v -> v.getKey(), v -> v.getValue()));
  }

  public Map<LocalDate, Map<String, Integer>> getValuesUntil(LocalDate endDate) {
    return values.entrySet().stream()
        .filter(v -> v.getKey().isBefore(endDate.plusDays(1)))
        .collect(Collectors.toMap(v -> v.getKey(), v -> v.getValue()));
  }

  public Map<LocalDate, Map<String, Integer>> getValuesBetween(LocalDate startDate, LocalDate endDate) {
    return values.entrySet().stream()
        .filter(v -> v.getKey().isAfter(startDate.minusDays(1)))
        .filter(v -> v.getKey().isBefore(endDate.plusDays(1)))
        .collect(Collectors.toMap(v -> v.getKey(), v -> v.getValue()));
  }
}
