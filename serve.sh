# Check if the necessary tools are installed
command -v tmux >/dev/null 2>&1 || { echo >&2 "I require tmux but it's not installed.  Aborting."; exit 1; }
command -v entr >/dev/null 2>&1 || { echo >&2 "I require entr but it's not installed.  Aborting."; exit 1; }
command -v lsof >/dev/null 2>&1 || { echo >&2 "I require lsof but it's not installed.  Aborting."; exit 1; }

# Kill existing session of the same name, and sleep a while to let threads die. Do this before port checks.
tmux kill-session -t cov && { echo "Waiting for existing session to close"; sleep 2; }


# Check if 'lsof' is available, and if so do port checks
lsof -i:8080 >/dev/null 2>&1 && { echo "Need port 8080 to be free. (perhaps another tmux session is hogging it? Try 'tmux kill-server'.)"; exit 1; }
lsof -i:8090 >/dev/null 2>&1 && { echo "Need port 8090 to be free. (perhaps another tmux session is hogging it? Try 'tmux kill-server'.)"; exit 1; }
lsof -i:3000 >/dev/null 2>&1 && { echo "Need port 3000 to be free. (perhaps another tmux session is hogging it? Try 'tmux kill-server'.)"; exit 1; }

# Navigate to scripts, they work with relative paths
cd scripts

# Launch a tmux session
echo "Launching tmux session 'cov'"
tmux new-session -d -s cov ./client.sh

# Let panes remain on exit, so that any crashes remain visible
tmux set-option remain-on-exit

tmux split-window -v -t 0 ./service.sh
tmux split-window -h ./webtiles.sh
tmux split-window -v -t 0 ./gateway.sh

# Attach to the session
tmux -2 attach-session -d
